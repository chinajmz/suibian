package com.ppnetwork.suixing.api.service;

import com.ppnetwork.suixing.api.endpoint.resource.ParkingHistoryResource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ParkingHistoryService {

    public List<ParkingHistoryResource> getListByUserId(long userId) {
        List<ParkingHistoryResource> parkingHistoryResources = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            parkingHistoryResources.add(
                new ParkingHistoryResource().setParkingLotName(i + "号停车场")
                    .setBegin(new Date())
                    .setUserId(userId)
            );
        }
        return parkingHistoryResources;
    }
}
