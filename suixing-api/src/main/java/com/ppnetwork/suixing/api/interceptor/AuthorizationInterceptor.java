package com.ppnetwork.suixing.api.interceptor;

import com.ppnetwork.suixing.api.endpoint.config.ApiScope;
import com.ppnetwork.suixing.api.endpoint.config.LoginRequired;
import com.ppnetwork.suixing.api.endpoint.resource.ApiContext;
import com.ppnetwork.suixing.api.model.User;
import com.ppnetwork.suixing.api.service.AuthService;
import com.ppnetwork.suixing.api.web.ApiException;
import com.ppnetwork.suixing.api.web.ApiExceptionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Component
public class AuthorizationInterceptor extends BaseInterceptor {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthorizationInterceptor.class);

    @Autowired
    private ApiContext apiContext;
    @Autowired
    private AuthService authService;

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HandlerMethod handlerMethod) throws ApiException {
        ApiScope apiScope = handlerMethod.getMethod().getAnnotation(ApiScope.class);
        boolean isLoginRequired = false;
        if (apiScope != null) {
            isLoginRequired = true;
        }
        //初始化请求信息
        initContext(httpServletRequest, isLoginRequired);

        if (LOGGER.isDebugEnabled()) {
            String requestURI = httpServletRequest.getRequestURI();
            LOGGER.debug(">>>>>request url:{}", requestURI);
        }

        if (apiScope == null) {
            LOGGER.debug(">>>>>公共资源");
            return true;
        }

        if (apiScope.isInternal()) {
            //todo 添加超级管理员权限
        }

        List<String> apiScopes = Arrays.asList(apiScope.value());
        Set<String> userScopes = apiContext.getUser().getScopes();

        if (apiScopes.isEmpty() && userScopes != null) {
            return true;
        }


        if (!verifyScope(userScopes, apiScopes)) {
            if (LOGGER.isDebugEnabled()) {
                String method = httpServletRequest.getMethod();
                LOGGER.debug(">>>>>权限不足,method:{},scopes:{}", method, apiScopes);
            }
            throw ApiExceptionFactory.apiAccessDenied();
        }

        return true;
    }

    private boolean verifyScope(Set<String> scopesInDB, List<String> scopesAPIRequired) {
        return !(scopesInDB == null || scopesInDB.isEmpty()) && scopesInDB.stream().anyMatch(scopesAPIRequired::contains);
    }

    private void initContext(HttpServletRequest request, boolean isLoginRequired) throws ApiException {
        String token = request.getHeader("Authorization");
        if (StringUtils.isEmpty(token)) {
            LOGGER.debug("没有登录信息");
            if (isLoginRequired) {
                throw ApiExceptionFactory.loginRequired();
            }
            return;
        }
        LOGGER.debug("Authorization:{}", token);
        User user = authService.getUser(token);
        apiContext.setUser(user);
    }
}
