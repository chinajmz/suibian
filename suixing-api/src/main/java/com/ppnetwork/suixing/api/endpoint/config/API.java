package com.ppnetwork.suixing.api.endpoint.config;

public interface API {
    interface UserScope {
        String WRITE = "user";
        String READONLY = "user.readonly";
    }

    interface TestScope {
        String WRITE = "test";
        String READONLY = "test.readonly";
    }
}
