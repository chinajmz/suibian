package com.ppnetwork.suixing.api.model;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class User {
    private long id;
    private String name;
    private Set<String> scopes = new HashSet<>();

    public long getId() {
        return id;
    }

    public User setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public Set<String> getScopes() {
        return scopes;
    }

    public User setScopes(Set<String> scopes) {
        this.scopes = scopes;
        return this;
    }

    public User addScope(String... scopes) {
        this.scopes.addAll(Arrays.asList(scopes));
        return this;
    }

    @Override
    public String toString() {
        return "User{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", scopes=" + scopes +
            '}';
    }
}
