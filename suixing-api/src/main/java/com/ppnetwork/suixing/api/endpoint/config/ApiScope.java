package com.ppnetwork.suixing.api.endpoint.config;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 使用方式
 * `@ApiScope` : 只需登录
 * `@ApiScope({"user","parkingHistory.readonly"})` 需要当前登录用户有访问"user"和"parkingHistory.readonly"的权限
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface ApiScope {
    /**
     * APi需要权限设置
     */
    String[] value() default {};

    /**
     * 是否是内部API，必须超管权限才可访问
     */
    boolean isInternal() default false;
}
