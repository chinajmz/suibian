package com.ppnetwork.suixing.api.endpoint.resource;

import java.util.HashSet;
import java.util.Set;

public class UserResource {
    private long id;
    private String name;
    private Set<String> scopes = new HashSet<>();

    public long getId() {
        return id;
    }

    public UserResource setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public UserResource setName(String name) {
        this.name = name;
        return this;
    }

    public Set<String> getScopes() {
        return scopes;
    }

    public UserResource setScopes(Set<String> scopes) {
        this.scopes = scopes;
        return this;
    }

    @Override
    public String toString() {
        return "UserResource{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", scopes=" + scopes +
            '}';
    }
}
