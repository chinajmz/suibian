package com.ppnetwork.suixing.api.endpoint;

import com.ppnetwork.suixing.api.endpoint.resource.ApiContext;
import com.ppnetwork.suixing.api.endpoint.resource.AuthResource;
import com.ppnetwork.suixing.api.endpoint.resource.UserResource;
import com.ppnetwork.suixing.api.model.User;
import com.ppnetwork.suixing.api.service.AuthService;
import com.ppnetwork.suixing.api.service.UserService;
import com.ppnetwork.suixing.api.web.ApiException;
import com.ppnetwork.suixing.api.web.ApiExceptionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("oauth")
public class AuthEndpoint {

    @Autowired
    private AuthService authService;
    @Autowired
    private UserService userService;
    @Autowired
    private ApiContext apiContext;

    @PostMapping("login")
    public AuthResource login(@RequestBody UserResource userResource) throws ApiException {
        User user = userService.get(userResource.getId());
        if (user == null) {
            throw ApiExceptionFactory.incorrectUser();
        }
        return authService.login(user);
    }

    @PostMapping("refresh")
    public AuthResource refresh(
        @RequestParam("token") String refreshToken
    ) throws ApiException {
        return authService.refresh(refreshToken);
    }

    @PostMapping("register")
    public AuthResource register(@RequestBody UserResource userResource) {
        return authService.register(userResource);
    }

    @GetMapping("info")
    public User info() {
        return apiContext.getUser();
    }
}
