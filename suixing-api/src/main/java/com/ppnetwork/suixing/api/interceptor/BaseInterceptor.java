package com.ppnetwork.suixing.api.interceptor;

import com.ppnetwork.suixing.api.web.ApiException;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class BaseInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        HandlerMethod handlerMethod = null;
        if (o instanceof HandlerMethod) {
            handlerMethod = (HandlerMethod) o;
        }
        return preHandle(httpServletRequest, handlerMethod);
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }

    public abstract boolean preHandle(HttpServletRequest httpServletRequest, HandlerMethod handlerMethod) throws ApiException;
}
