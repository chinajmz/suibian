package com.ppnetwork.suixing.api.web;

import org.springframework.http.HttpStatus;

public class ApiException extends Exception {
    private final HttpStatus status;
    private final String error;
    private final String errorDescription;

    public ApiException(String error,String errorDetail,HttpStatus status){
        this.error = error;
        this.errorDescription = errorDetail;
        this.status = status;
    }

    public HttpStatus getStatusCode() {
        return status;
    }

    public String getError() {
        return error;
    }

    public String getErrorDescription() {
        return errorDescription;
    }
}
