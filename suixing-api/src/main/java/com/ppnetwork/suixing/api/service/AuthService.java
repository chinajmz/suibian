package com.ppnetwork.suixing.api.service;

import com.ppnetwork.suixing.api.endpoint.resource.AuthResource;
import com.ppnetwork.suixing.api.endpoint.resource.UserResource;
import com.ppnetwork.suixing.api.model.User;
import com.ppnetwork.suixing.api.web.ApiException;
import com.ppnetwork.suixing.api.web.ApiExceptionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class AuthService {
    /**
     * 模拟仓库，应该每隔30分钟失效
     */
    private static final Map<String, Long> accessTokenMaps = new HashMap<>();
    /**
     * 模拟仓库，实际上应该保存到数据库
     */
    private static final Map<String, Long> refreshTokenMaps = new HashMap<>();

    @Autowired
    private UserService userService;

    public User getUser(String accessToken) throws ApiException {
        long userId = accessTokenMaps.getOrDefault(accessToken, 0L);

        if (userId > 0) {
            return userService.get(userId);
        }
        throw ApiExceptionFactory.invalidAccessToken();
    }

    public AuthResource login(User user) {
        String accessToken = UUID.randomUUID().toString();
        String refreshToken = UUID.randomUUID().toString();

        accessTokenMaps.put(accessToken, user.getId());
        refreshTokenMaps.put(refreshToken, user.getId());

        return new AuthResource()
            .setAccessToken(accessToken)
            .setRefreshToken(refreshToken)
            .setExpiredIn(1800);
    }

    public AuthResource refresh(String refreshToken) throws ApiException {
        long userId = refreshTokenMaps.getOrDefault(refreshToken, 0L);

        if (userId > 0) {
            String accessToken = UUID.randomUUID().toString();
            accessTokenMaps.put(accessToken, userId);

            return new AuthResource()
                .setAccessToken(accessToken)
                .setExpiredIn(1800);
        }
        throw ApiExceptionFactory.invalidAccessToken();
    }

    public AuthResource register(UserResource userResource) {
        //TODO 客户端注册用户
        return null;
    }
}
