package com.ppnetwork.suixing.api.service;

import com.ppnetwork.suixing.api.endpoint.config.API;
import com.ppnetwork.suixing.api.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class UserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    private static final Map<Long, User> users = new HashMap<>();

    static {
        User user = new User();
        user.setId(1)
            .setName("admin")
            .addScope(
                API.TestScope.READONLY,
                API.UserScope.WRITE
            );
        users.put(user.getId(), user);
    }

    public User create(User user, User creator) {
        LOGGER.debug("Creator:{}", creator.getName());
        users.put(user.getId(), user);
        return user;
    }

    public User patch(long id, Map<String, Object> params) {
        return null;
    }

    public User update(long id, User user) {
        users.put(id, user);
        return user;
    }

    public Page<User> list() {
        return null;
    }

    public User get(long id) {
        return users.get(id);
    }
}
