package com.ppnetwork.suixing.api.endpoint;

import com.ppnetwork.suixing.api.endpoint.config.API;
import com.ppnetwork.suixing.api.endpoint.config.ApiScope;
import com.ppnetwork.suixing.api.endpoint.config.LoginRequired;
import com.ppnetwork.suixing.api.endpoint.resource.ApiContext;
import com.ppnetwork.suixing.api.endpoint.resource.ParkingHistoryResource;
import com.ppnetwork.suixing.api.model.User;
import com.ppnetwork.suixing.api.service.ParkingHistoryService;
import com.ppnetwork.suixing.api.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserEndpoint {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserEndpoint.class);

    @Autowired
    private UserService userService;
    @Autowired
    private ParkingHistoryService parkingHistoryService;
    @Autowired
    private ApiContext apiContext;

    @ApiScope({
        API.UserScope.READONLY,
        API.UserScope.WRITE
    })
    @RequestMapping(value = "{userId}", method = RequestMethod.GET)
    public User get(@PathVariable long userId) {
        LOGGER.debug("当前访问用户：{}", apiContext.getUser().getName());
        return userService.get(userId);
    }

    @ApiScope({
        API.UserScope.WRITE
    })
    @RequestMapping(method = RequestMethod.GET)
    public Page<User> getList() {
        return userService.list();
    }

    @ApiScope({
        API.UserScope.WRITE
    })
    @RequestMapping(value = "{userId}", method = RequestMethod.PUT)
    public User update(@PathVariable long userId, @RequestBody User user) {
        return userService.update(userId, user);
    }

    @ApiScope({
        API.UserScope.WRITE
    })
    @RequestMapping(value = "{userId}", method = RequestMethod.PATCH)
    public User patch(@PathVariable long userId, @RequestBody Map<String, Object> params) {
        return userService.patch(userId, params);
    }

    @ApiScope({
        API.UserScope.WRITE
    })
    @RequestMapping(method = RequestMethod.POST)
    public User post(@RequestBody User user) {
        return userService.create(user, apiContext.getUser());
    }

    //TODO 超级管理员权限
    @ApiScope(isInternal = true)
    @GetMapping("{userId}/parkingHistory")
    public List<ParkingHistoryResource> getParkingHistory(@PathVariable long userId) {
        return parkingHistoryService.getListByUserId(userId);
    }

    @ApiScope
    @GetMapping("current/parkingHistory")
    public List<ParkingHistoryResource> getCurrentUserParkingHistory() {
        return parkingHistoryService.getListByUserId(apiContext.getUser().getId());
    }
}
