package com.ppnetwork.suixing.api.endpoint.resource;

public class AuthResource {
    private String accessToken;
    private String refreshToken;
    private long expiredIn;

    public String getAccessToken() {
        return accessToken;
    }

    public AuthResource setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        return this;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public AuthResource setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
        return this;
    }

    public long getExpiredIn() {
        return expiredIn;
    }

    public AuthResource setExpiredIn(long expiredIn) {
        this.expiredIn = expiredIn;
        return this;
    }

    @Override
    public String toString() {
        return "AuthResource{" +
            "accessToken='" + accessToken + '\'' +
            ", refreshToken='" + refreshToken + '\'' +
            ", expiredIn=" + expiredIn +
            '}';
    }
}
