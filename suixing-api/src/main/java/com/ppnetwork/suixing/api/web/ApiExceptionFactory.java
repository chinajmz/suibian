package com.ppnetwork.suixing.api.web;

import org.springframework.http.HttpStatus;

public final class ApiExceptionFactory {
    private ApiExceptionFactory() {
    }

    public static ApiException apiAccessDenied() {
        return new ApiException("access_denied", "没有访问此API的权限", HttpStatus.FORBIDDEN);
    }

    public static ApiException invalidAccessToken() {
        return new ApiException("invalid_token", "AccessToken已失效", HttpStatus.FORBIDDEN);
    }

    public static ApiException incorrectUser() {
        return new ApiException("incorrect_user", "用户不存在或密码错误", HttpStatus.BAD_REQUEST);
    }

    public static ApiException loginRequired() {
        return new ApiException("login_required", "请登录后重试", HttpStatus.UNAUTHORIZED);
    }
}
