package com.ppnetwork.suixing.api.endpoint.resource;

import java.util.Date;

public class ParkingHistoryResource {
    private String parkingLotName;
    private Date begin;
    private long userId;

    public String getParkingLotName() {
        return parkingLotName;
    }

    public ParkingHistoryResource setParkingLotName(String parkingLotName) {
        this.parkingLotName = parkingLotName;
        return this;
    }

    public Date getBegin() {
        return begin;
    }

    public ParkingHistoryResource setBegin(Date begin) {
        this.begin = begin;
        return this;
    }

    public long getUserId() {
        return userId;
    }

    public ParkingHistoryResource setUserId(long userId) {
        this.userId = userId;
        return this;
    }

    @Override
    public String toString() {
        return "ParkingHistoryResource{" +
            "parkingLotName='" + parkingLotName + '\'' +
            ", begin=" + begin +
            ", userId=" + userId +
            '}';
    }
}
