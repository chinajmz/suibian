package com.ppnetwork.suixing.api.endpoint.resource;

import com.ppnetwork.suixing.api.model.User;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

/**
 * 为了代码逻辑清晰，不该把Context注入到Service层及其后
 */
@RequestScope
@Component
public class ApiContext {

    private User user;

    public User getUser() {
        return user;
    }

    public ApiContext setUser(User user) {
        this.user = user;
        return this;
    }

}
