package com.ppnetwork.suixing.api.web;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class WebControllerAdvice {
    @ExceptionHandler({ApiException.class})
    @ResponseBody
    public ResponseEntity<Map<String, String>> handleIOException(ApiException ex) {
        Map<String, String> body = new HashMap<>();
        body.put("error", ex.getError());
        body.put("error_description", ex.getErrorDescription());
        return new ResponseEntity<>(body, ex.getStatusCode());
    }
}
